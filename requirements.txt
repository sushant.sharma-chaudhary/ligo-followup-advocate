astropy
humanize
jinja2
ligo-gracedb >= 1.29.dev1
ligo.skymap >= 1.1.1
lxml
numpy
